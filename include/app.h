/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 * ----------------------------------------------------------------------------
 * app.h
 * - Overall application header file for the Blinky sample
 *   application
 * ------------------------------------------------------------------------- */

#ifndef APP_H
#define APP_H

/* ----------------------------------------------------------------------------
 * Include files
 * --------------------------------------------------------------------------*/
#include <rsl10.h>
#include <rsl10_flash_rom.h>

/* ----------------------------------------------------------------------------
 * Define declaration
 * ------------------------------------------------------------------------- */

/* DIO number that is connected to LED of EVB */
#define LED_DIO                         13

/* ----------------------------------------------------------------------------
 * Interrupt handler declaration
 * ------------------------------------------------------------------------- */
extern void DIO0_IRQHandler(void);

/* ----------------------------------------------------------------------------
 * Forward declaration
 * ------------------------------------------------------------------------- */
void Initialize(void);

#endif    /* APP_H_ */
