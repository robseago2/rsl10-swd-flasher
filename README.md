# README #

Flasher is used with openocd to program an RSL10 image using the SWD port. 
openocd scripts load flasher.hex to RSL10 PRAM, get flasher to erase the 
RSL10 flash, and then chunks of code are sent over SWD to RSL10 RAM which 
flasher programs into flash.

### How do I get set up? ###

* On a Windows PC, clone flasher into a new folder
* Open the On-Semi IDE
* Choose a new workspace folder (different to the folder containing the source)
* File->Import->General->Existing Projects into Workspace
* Next
* Select the rsl10-swd-flasher folder that was cloned
* flasher project should be selected to import
* Finish

### Build ###

* Make any changes
* Project->Build All
* flasher.hex will be in the Debug folder
