/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 * - Simple application using a DIO5 input to control a DIO6 output
 * ----------------------------------------------------------------------------
 * $Revision: 1.11 $
 * $Date: 2019/11/11 17:45:11 $
 * ------------------------------------------------------------------------- */

#include "app.h"

/* ----------------------------------------------------------------------------
 * Function      : void DIO0_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Toggle the toggle status global flag.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DIO0_IRQHandler(void)
{

}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system by disabling interrupts, configuring
 *                 the required DIOs and DIO interrupt,
 *                 updating SystemCoreClockUpdate and enabling interrupts.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all existing interrupts, clearing all pending source */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Configure the current trim settings for VCC, VDDA */
    ACS_VCC_CTRL->ICH_TRIM_BYTE = VCC_ICHTRIM_16MA_BYTE;
    ACS_VDDA_CP_CTRL->PTRIM_BYTE = VDDA_PTRIM_16MA_BYTE;

    /* Start 48 MHz XTAL oscillator */
    ACS_VDDRF_CTRL->ENABLE_ALIAS = VDDRF_ENABLE_BITBAND;
    ACS_VDDRF_CTRL->CLAMP_ALIAS = VDDRF_DISABLE_HIZ_BITBAND;

    /* Wait until VDDRF supply has powered up */
    while (ACS_VDDRF_CTRL->READY_ALIAS != VDDRF_READY_BITBAND);

    ACS_VDDPA_CTRL->ENABLE_ALIAS = VDDPA_DISABLE_BITBAND;
    ACS_VDDPA_CTRL->VDDPA_SW_CTRL_ALIAS = VDDPA_SW_VDDRF_BITBAND;

    /* Enable RF power switches */
    SYSCTRL_RF_POWER_CFG->RF_POWER_ALIAS = RF_POWER_ENABLE_BITBAND;

    /* Remove RF isolation */
    SYSCTRL_RF_ACCESS_CFG->RF_ACCESS_ALIAS = RF_ACCESS_ENABLE_BITBAND;

    /* Start the 48 MHz oscillator without changing the other register bits */
    RF->XTAL_CTRL = ((RF->XTAL_CTRL & ~XTAL_CTRL_DISABLE_OSCILLATOR) |
                     XTAL_CTRL_REG_VALUE_SEL_INTERNAL);

    /* Enable the 48 MHz oscillator divider using the desired prescale value */
    RF_REG2F->CK_DIV_1_6_CK_DIV_1_6_BYTE = CK_DIV_1_6_PRESCALE_6_BYTE;

    /* Wait until 48 MHz oscillator is started */
    while (RF_REG39->ANALOG_INFO_CLK_DIG_READY_ALIAS !=
           ANALOG_INFO_CLK_DIG_READY_BITBAND);

    /* Switch to (divided 48 MHz) oscillator clock */
    Sys_Clocks_SystemClkConfig(JTCK_PRESCALE_1   |
                               EXTCLK_PRESCALE_1 |
                               SYSCLK_CLKSRC_RFCLK);

    /* Configure clock dividers */
    CLK->DIV_CFG0 = (SLOWCLK_PRESCALE_8 | BBCLK_PRESCALE_1 |
                     USRCLK_PRESCALE_1);
    CLK->DIV_CFG2 = (CPCLK_PRESCALE_8 | DCCLK_PRESCALE_2);

    BBIF->CTRL = (BB_CLK_ENABLE | BBCLK_DIVIDER_8 | BB_WAKEUP);

    DIO_JTAG_SW_PAD_CFG->CM3_JTAG_DATA_EN_ALIAS = CM3_JTAG_DATA_DISABLED_BITBAND;
    DIO_JTAG_SW_PAD_CFG->CM3_JTAG_TRST_EN_ALIAS = CM3_JTAG_TRST_DISABLED_BITBAND;

    Sys_DIO_Config(LED_DIO, DIO_MODE_GPIO_OUT_0);
    Sys_GPIO_Set_High(LED_DIO);

    /* Unmask all interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);
}

/* utility function for a microsecond sleep - will overrun at 90 seconds */
void usleep(uint32_t usec)
{
    uint32_t ClockMhz = SystemCoreClock / 1000000;
    Sys_Delay_ProgramROM(ClockMhz * usec);
}

#define NOINIT_SECT __attribute__((section(".noinit")))

typedef struct {
    /* inputs */
    uint32_t marker;
    uint32_t cmd;
    uint32_t src_addr;
    uint32_t dst_addr;
    uint32_t wlen; // 32-bit words
    /* outputs */
    uint32_t status;
}mailbox_t;

/* known address */
mailbox_t Mailbox NOINIT_SECT;

#define MB_MARKER 0x55ff55ff

#define MB_CMD_DELAY         0x10000001
#define MB_CMD_ERASE_FLASH   0x10000002
#define MB_CMD_WRITE_BUFFER  0x10000003
#define MB_CMD_WRITE_NVR     0x10000004
#define MB_CMD_CHECKSUM      0x10000005
#define MB_CMD_REBOOT        0x10000006

#define MB_CMD_COMPLETE      0x2000000C

#if 0
/* Flash error codes */
typedef enum
{
    FLASH_ERR_NONE                      = 0x0,
    FLASH_ERR_GENERAL_FAILURE           = 0x1,
    FLASH_ERR_WRITE_NOT_ENABLED         = 0x2,
    FLASH_ERR_BAD_ADDRESS               = 0x3,
    FLASH_ERR_ERASE_FAILED              = 0x4,
    FLASH_ERR_BAD_LENGTH                = 0x5,
    FLASH_ERR_INACCESSIBLE              = 0x6,
    FLASH_ERR_COPIER_BUSY               = 0x7,
    FLASH_ERR_PROG_FAILED               = 0x8
} FlashStatus;
#endif

/* see KB: Setting the Flash Delay to Ensure Read & Write Operations are Successful */

/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    mailbox_t *mailbox = &Mailbox;
    uint32_t flshdelay;
    uint32_t ClockMhz;

    memset(mailbox, 0, sizeof(mailbox_t));

    /* Initialize the system - set to 48MHz clock */
    Initialize();

    /* Spin loop */
    while (1)
    {
        /* Refresh the watchdog timer */
        Sys_Watchdog_Refresh();

        if(mailbox->marker == MB_MARKER) {
            mailbox->marker = 0x00000000;
            Sys_GPIO_Toggle(LED_DIO);
            switch (mailbox->cmd) {
                case MB_CMD_DELAY:
                    mailbox->cmd = 0;
                    mailbox->status = 0xffffffff;
                    uint32_t delay_ms = 0;
                    uint32_t ms = mailbox->wlen;
                    while(delay_ms < ms) {
                        usleep(10000); // 10 ms
                        Sys_Watchdog_Refresh();
                        delay_ms += 10;
                    }
                    mailbox->status = 0x00;
                    mailbox->cmd = MB_CMD_COMPLETE;
                    break;
                case MB_CMD_ERASE_FLASH:
                    mailbox->cmd = 0;
                    mailbox->status = 0xffffffff;
                    FLASH->MAIN_CTRL = (MAIN_LOW_W_ENABLE | MAIN_MIDDLE_W_ENABLE | MAIN_HIGH_W_ENABLE);
                    FLASH->MAIN_WRITE_UNLOCK = FLASH_MAIN_KEY;
//                    flshdelay = FLASH->DELAY_CTRL;
                    flshdelay = 0x09; // Flash delay as suggested in [KB: Setting the Flash Delay to Ensure Read & Write Operations are Successful]
                    FLASH->DELAY_CTRL = flshdelay;
                    /* Erase the main flash */
                    mailbox->status = Flash_EraseAll();
                    FLASH->MAIN_CTRL = (MAIN_LOW_W_DISABLE | MAIN_MIDDLE_W_DISABLE | MAIN_HIGH_W_DISABLE);
                    FLASH->MAIN_WRITE_UNLOCK = FLASH_MAIN_KEY;
                    mailbox->cmd = MB_CMD_COMPLETE;
                    break;
                case MB_CMD_WRITE_BUFFER:
                    mailbox->cmd = 0;
                    mailbox->status = 0xffffffff;
                    FLASH->MAIN_CTRL = (MAIN_LOW_W_ENABLE | MAIN_MIDDLE_W_ENABLE | MAIN_HIGH_W_ENABLE);
                    FLASH->MAIN_WRITE_UNLOCK = FLASH_MAIN_KEY;
//                    flshdelay = FLASH->DELAY_CTRL;
                    flshdelay = 0x09; // Flash delay as suggested in [KB: Setting the Flash Delay to Ensure Read & Write Operations are Successful]
                    FLASH->DELAY_CTRL = flshdelay;
                    /* Write chunk from RAM to flash */
                    unsigned int *pSrc;
                    pSrc = (unsigned int *)mailbox->src_addr;
                    mailbox->status = Flash_WriteBuffer(mailbox->dst_addr, mailbox->wlen, pSrc);
                    FLASH->MAIN_CTRL = (MAIN_LOW_W_DISABLE | MAIN_MIDDLE_W_DISABLE | MAIN_HIGH_W_DISABLE);
                    FLASH->MAIN_WRITE_UNLOCK = FLASH_MAIN_KEY;
                    mailbox->cmd = MB_CMD_COMPLETE;
                    break;
                case MB_CMD_WRITE_NVR:
#if 0
                    mailbox->cmd = 0;
                    mailbox->status = 0xffffffff;

                    FLASH->MAIN_WRITE_UNLOCK = FLASH_MAIN_KEY;
                    FLASH->MAIN_CTRL = (MAIN_LOW_W_ENABLE | MAIN_MIDDLE_W_ENABLE | MAIN_HIGH_W_ENABLE);
                    /* Write chunk from RAM to flash */
                    unsigned int *pSrc;
                    pSrc = (unsigned int *)mailbox->src_addr;
                    mailbox->status = Flash_WriteBuffer(mailbox->dst_addr, mailbox->wlen, pSrc);
                    mailbox->cmd = MB_CMD_COMPLETE;
#endif
                    break;
                case MB_CMD_CHECKSUM:
                    mailbox->cmd = 0;
                    mailbox->status = 0xffffffff;
                    uint32_t *pAddr;
                    uint32_t wlen = mailbox->wlen;
                    uint32_t checksum = 0;
                    pAddr = (uint32_t *)mailbox->src_addr;
                    while(wlen > 0) {
                        checksum += *pAddr;
                        pAddr++;
                        wlen--;
                    }
                    mailbox->status = checksum;
                    mailbox->cmd = MB_CMD_COMPLETE;
                    break;
                case MB_CMD_REBOOT:
                    Sys_GPIO_Set_Low(LED_DIO);
                    while(1);
                    break;
            }
        }

        usleep(10000); // 10 ms
    }
}
